package com.gaul.minimongo.core;

import java.util.List;
import java.util.Map;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Test
 */
public class MiniMongoTest {
	private static final String DATABASE_NAME = "mini-mongodb.xml";
	
	public static void main(String[] args) {
		MiniMongoTest mongo = new MiniMongoTest();
//		mongo.createDataBase();//创建数据库 表
//		mongo.insertData();
//		mongo.findData();
//		mongo.updateData();
		mongo.deleteData();
	}
	
	/**
	 * 创建数据库 表
	 */
	public void createDataBase() {
		try{
			MiniMongodb dao = new MiniMongodb();
			dao.createDataBase(DATABASE_NAME, "test");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * 添加数据
	 */
	public void insertData() {
		Person person = new Person();
		person.setName("lisi");
		person.setAge(20);
		person.setSex("female");
		person.setMoney(5000.13);
		try{
			MiniMongodb dao = new MiniMongodb();
			dao.addData(DATABASE_NAME, "test", person);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * 查询数据
	 */
	public void findData() {
		try {
			MiniMongodb dao = new MiniMongodb();
			List<Map> data = dao.loadTableDatas(DATABASE_NAME, "test");
			System.out.println("data: " + data);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 修改数据
	 */
	public void updateData() {
		Person person = new Person();
		person.set_uuid("b903874c-07bd-40da-98a2-39a924ef712a");
		person.setMoney(222.22);
		try {
			MiniMongodb dao = new MiniMongodb();
			dao.updateData(DATABASE_NAME, "test", person);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 删除数据
	 */
	public void deleteData() {
		Person person = new Person();
		person.set_uuid("b903874c-07bd-40da-98a2-39a924ef712a");
		try {
			MiniMongodb dao = new MiniMongodb();
			dao.deleteData(DATABASE_NAME, "test", person);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
